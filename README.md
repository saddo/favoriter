# Favoriter

## What does it do ?
It patches `profiles.dat` and adds to the first profile's favorites the 4
troll-game servers. Note: if you have multiple profiles, the servers will only
be added for the first one.

## How can I use it ?
1. download the executable, and double-click it. It will look for `profiles.dat`
   in the current user's `%APPDATA%\Mount&Blade Warband` folder. If it can't find
   it there, it will look in it's current directory. Once found, it will automatically
   create a backup, and then will patch the first profile by adding troll-game servers
   to favories.
