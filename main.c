#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <conio.h>
 
struct Server
{
    uint32_t ip_length;
    char* ip;
    uint32_t port;
};

uint8_t zero8[1024];
uint32_t zero32[32];

/* 
 * based on:  http://stackoverflow.com/questions/10467711/c-write-in-the-middle-of-a-binary-file-without-overwriting-any-existing-content
 * - converted to ANSI C
 * - will only extend the file to accomodate new `inslen` bytes, but will not
 *   write anything - the "cave" created will be zeroed out
 */
static int extend_file(FILE* f, off_t offset, size_t inslen)
{
    enum { BUFFERSIZE = 64 * 1024 };

    #define MIN(x, y) (((x) < (y)) ? (x) : (y))

    char buffer[BUFFERSIZE];
    int rc = -1;

    /* get file size*/
    fseek(f, 0, SEEK_END);
    size_t size = ftell(f);
    fseek(f, 0, SEEK_SET);

    if (size > offset)
    {
        /* Move data after offset up by inslen bytes */
        size_t bytes_to_move = size - offset;
        off_t read_end_offset = size; 
        while (bytes_to_move != 0)
        {
            ssize_t bytes_this_time = MIN(BUFFERSIZE, bytes_to_move);
            ssize_t read_offset = read_end_offset - bytes_this_time;
            ssize_t write_offset = read_offset + inslen;
            fseek(f, read_offset, SEEK_SET);
            if (fread(buffer, 1, bytes_this_time, f) != bytes_this_time)
                return -1;
            fseek(f, write_offset, SEEK_SET);
/*            printf("writing moved bytes at: 0x%04x\n", write_offset);*/
            if (fwrite(buffer, 1, bytes_this_time, f) != bytes_this_time)
                return -1;
            bytes_to_move -= bytes_this_time;
            read_end_offset -= bytes_this_time; /* Added 2013-07-19 */
        }   
        fseek(f, offset, SEEK_SET);
        fwrite(&zero8, 1, inslen, f);
    }   
    rc = 0;
    return rc;
}

void serialize_byte_array(FILE* f, uint32_t* p_bytes, size_t nr_of_bytes)
{
    fwrite(&nr_of_bytes, 4, 1, f);
    fwrite(p_bytes, nr_of_bytes, 1, f);
}

/*
 * Write servers into profile.dat
 *
 * offset - nr of bytes from start of file until first favorited server
 */
int write_struct(FILE* f, size_t offset, struct Server* server)
{
    size_t size = 1*4 + server->ip_length + 4*3 + 4*2 + 4 + 4*4 + 4*8 + 4;

/*    printf("extending file by %d bytes at 0x%04x ...\n", size, offset);*/
    int res = extend_file(f, offset, size);
    if(res != 0)
    {
        printf("error extending file - exiting...\n", size);
        return 1;
    }

    printf("Adding server: %s:%d\n", server->ip, server->port);

    fseek(f, offset, SEEK_SET);
    serialize_byte_array(f, (uint32_t*)server->ip, server->ip_length); // ip_length, ip
    fwrite(&zero32, 4, 1, f); // ping
    fwrite(&server->port, 4, 1, f); // port
    fwrite(&zero32, 4, 1, f); // site_id
    fwrite(&zero32, 4, 2, f); // steam_id
    fwrite(&zero32, 4, 1, f); // mission_template_id
    fwrite(&zero32, 4, 4, f); // zero-lenght strings
    fwrite(&zero32, 4, 8, f); // other crap
    serialize_byte_array(f, &zero32[0], 0); // module_settings
    return size;
}

/*
 * Copy `old_filename` to `new_filename`
 * from: http://www.codingunit.com/c-tutorial-copying-a-file-in-c
 */
int copy_file(char *old_filename, char  *new_filename)
{
    FILE  *ptr_old, *ptr_new;
    errno_t err = 0, err1 = 0;
    int  a;

    err = fopen_s(&ptr_old, old_filename, "rb");
    err1 = fopen_s(&ptr_new, new_filename, "wb");

    if(err != 0)
        return  -1;

    if(err1 != 0)
    {
        fclose(ptr_old);
        return  -1;
    }

    while(1)
    {
        a  =  fgetc(ptr_old);

        if(!feof(ptr_old))
            fputc(a, ptr_new);
        else
            break;
    }

    fclose(ptr_new);
    fclose(ptr_old);
    return  0;
}

void fill_struct(struct Server* server, uint32_t ip_lenght, char* ip, uint32_t port)
{
    server->ip_length = ip_lenght;
    server->ip = ip;
    server->port = port;
}

struct Server servers[4];

int main()
{
    printf("Favoriter v0.2\n");

    // fill the array of structs with info for TG servers
    fill_struct(&servers[0], 12, "85.10.198.71", 7244); // arena
    fill_struct(&servers[1], 12, "85.10.198.71", 7240); // siege
    fill_struct(&servers[2], 12, "85.10.198.71", 7250); // battle
    fill_struct(&servers[3], 12, "85.10.198.71", 7252); // duel

    // find profiles.dat - look in %APPDATA\Mount&Blade Warband or current directory
    char *profiles_path;
    char *app_data = getenv ("APPDATA");
    char *cwd = (char*) malloc(512);
    getcwd(cwd, 512);
    asprintf(&profiles_path, "%s%s", app_data, "\\Mount&Blade Warband\\profiles.dat");
    if (access(profiles_path, F_OK) == -1)
    {
        printf("Cannot access profiles.dat in APPDATA - looking in current directory...\n");
        free(profiles_path);
        asprintf(&profiles_path, "%s%s", cwd, "\\profiles.dat");
        if (access(profiles_path, F_OK) != -1)
        {
            printf("Found: %s\n", profiles_path);
        }
        else
        {
            printf("Cannot find profiles.dat - please copy it to this folder (%s)\nand then try running this program again.\n", cwd);
            return 1;
        }
    }

    // create backup
   time_t rawtime;
   struct tm * timeinfo;
   time (&rawtime);
   timeinfo = localtime (&rawtime);
   char *backup_name = (char*) malloc(128);
   strftime (backup_name, 128, "profiles.dat-backup-%Y.%m.%d-%I.%M.%S", timeinfo);
   char *backup_full_path;
   asprintf(&backup_full_path, "%s\\%s", cwd, backup_name);
   printf("Creating backup: %s...\n", backup_full_path);
   if(copy_file(profiles_path, backup_name) != 0)
   {
       fprintf(stderr, "Error during copy!");
       return 1;
   }
   free(backup_full_path);

    // open profiles.dat
    printf("Opening profiles.dat: \"%s\"\n", profiles_path);
    FILE *ptr_profile = fopen(profiles_path, "r+b");
    if (!ptr_profile)
    {
        printf("Error: unable to open file. Exiting...");
        return 1;
    }

    // calculate offset to byte with nr of faves
    size_t first_profile_name_length;
    fseek(ptr_profile, 3*4, SEEK_SET);
    fread(&first_profile_name_length, 4, 1, ptr_profile);
    size_t nr_favorited_offset = 3*4 + 4 + first_profile_name_length + 4 + 4 + 8*4;

    // get and print the name of the profile which will be updated
    char *bytes = (char*) malloc(first_profile_name_length*sizeof(char)+1);
    fread(bytes, 1, first_profile_name_length, ptr_profile);
    bytes[first_profile_name_length] = '\0';
    printf("Adding favorites to profile: %s\n", bytes);

    // get number of favorites
    size_t num_favorites;
    fseek(ptr_profile, nr_favorited_offset, SEEK_SET);
    fread(&num_favorites, 4, 1, ptr_profile);
/*    printf("num_favorites: %d\n", num_favorites);*/

    // update the number of favorites
    uint32_t new_num_favorites = num_favorites + 4;
    fseek(ptr_profile, nr_favorited_offset, SEEK_SET);
    fwrite(&new_num_favorites, 4, 1, ptr_profile);

    // check that it was updated
    fseek(ptr_profile, nr_favorited_offset, SEEK_SET);
    fread(&num_favorites, 4, 1, ptr_profile);
/*    printf("new_num_favorites: %d\n", num_favorites);*/

    // write favorite servers
    // ugly; should use file cursor
    size_t s1_size = write_struct(ptr_profile, nr_favorited_offset+4, &servers[0]);
    size_t s2_size = write_struct(ptr_profile, nr_favorited_offset+4 + s1_size, &servers[1]);
    size_t s3_size = write_struct(ptr_profile, nr_favorited_offset+4 + s1_size + s2_size, &servers[2]);
    size_t s4_size = write_struct(ptr_profile, nr_favorited_offset+4 + s1_size + s2_size + s3_size, &servers[3]);

    free(ptr_profile);
    fclose(ptr_profile);

    printf("Press any key to exit.");
    _getch();
}
